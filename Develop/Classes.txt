Classes in Hiearchy:

TObject
  Size: 4(b)
  Used in Modules:
     TStressAppFormUnit.pas
  Sub Classes:
    TCustomIniFile
      Size: 8(b)
      Sub Classes:
        TIniFile
          Size: 8(b)
          Sub Classes:
            TGenIniFile
              Size: 8(b)
    TGenAppPropBase
      Size: 20(b)
      Sub Classes:
        TGenAppPropBool
          Size: 24(b)
        TGenAppPropColor
          Size: 24(b)
        TGenAppPropCustomColors
          Size: 28(b)
        TGenAppPropDouble
          Size: 32(b)
        TGenAppPropFont
          Size: 24(b)
        TGenAppPropInt
          Size: 24(b)
        TGenAppPropPoint
          Size: 28(b)
        TGenAppPropRect
          Size: 36(b)
          Used in Modules:
             TStressAppFormUnit.pas
        TGenAppPropString
          Size: 24(b)
        TGenAppPropStringList
          Size: 24(b)
    TGenAppPropList
      Size: 8(b)
    TGenCoreTemp
      Size: 24(b)
    TGenHsv
      Size: 16(b)
    TGenLog
      Size: 480(b)
      Used in Modules:
         TStressAppFormUnit.pas
    TGenMonObj
      Size: 48(b)
      Sub Classes:
        TCpuObj
          Size: 116(b)
          Used in Modules:
             TStressAppFormUnit.pas
    TGenObject
      Size: 4(b)
      Sub Classes:
        TGenClasses
          Size: 4(b)
          Used in Modules:
             TMyLoadThreadUnit.pas
             TStressAppFormUnit.pas
        TMyClass
          Size: 32(b)
        TMyClassList
          Size: 8(b)
        TMyModule
          Size: 32(b)
          Used in Modules:
             TMyLoadThreadUnit.pas
             TStressAppFormUnit.pas
        TMyModuleList
          Size: 20(b)
        TMySymbol
          Size: 20(b)
        TMySymbolList
          Size: 8(b)
    TGenTextFile
      Size: 476(b)
    TGenThreadQueue
      Size: 12(b)
    THsvBaseFrame
      Size: 44(b)
      Sub Classes:
        THsvHueCircleFrame
          Size: 68(b)
        THsvHueFrame
          Size: 44(b)
        THsvSatFrame
          Size: 44(b)
        THsvSatValFrame
          Size: 44(b)
        THsvValueFrame
          Size: 44(b)
    THsvHueSatFrame
      Size: 44(b)
    TList
      Size: 16(b)
      Sub Classes:
        TObjectList
          Size: 20(b)
          Sub Classes:
            TDebugObjectList
              Size: 20(b)
    TPersistent
      Size: 4(b)
      Sub Classes:
        TComponent
          Size: 48(b)
          Sub Classes:
            TControl
              Size: 348(b)
              Sub Classes:
                TWinControl
                  Size: 516(b)
                  Sub Classes:
                    TCustomEdit
                      Size: 544(b)
                      Sub Classes:
                        TCustomMemo
                          Size: 556(b)
                          Sub Classes:
                            TCustomRichEdit
                              Size: 648(b)
                              Sub Classes:
                                TGenRtfFile
                                  Size: 664(b)
                    TScrollingWinControl
                      Size: 540(b)
                      Sub Classes:
                        TCustomForm
                          Size: 760(b)
                          Sub Classes:
                            TForm
                              Size: 760(b)
                              Used in Modules:
                                 TStressAppFormUnit.pas
                              Sub Classes:
                                TGenColorPick
                                  Size: 2248(b)
                                TGenPickFont
                                  Size: 828(b)
                                TGenProgressWin
                                  Size: 780(b)
                                TStressAppForm
                                  Size: 864(b)
                                  Module: TStressAppFormUnit.pas
                                  Lines: 299
    TThread
      Size: 60(b)
      Used in Modules:
         TMyLoadThreadUnit.pas
      Sub Classes:
        TGenThread
          Size: 80(b)
          Sub Classes:
            TCpuSpeedThread
              Size: 84(b)
        TMyLoadThread
          Size: 76(b)
          Module: TMyLoadThreadUnit.pas
          Lines: 78
          Used in Modules:
             TStressAppFormUnit.pas
