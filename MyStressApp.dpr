program MyStressApp;

uses
  Forms,
  Windows,
  TStressAppFormUnit in 'TStressAppFormUnit.pas' {StressAppForm},
  TMyLoadThreadUnit in 'TMyLoadThreadUnit.pas';

{$R *.res}

var
  Sem  : THandle;
begin

  Sem := Windows.CreateSemaphore(nil, 0, 1, 'TGenStressApp');
  if ((Sem <> 0) and (GetLastError = ERROR_ALREADY_EXISTS)) then
    begin
      Windows.MessageBeep(MB_ICONHAND);
      Halt;
    end;

  Application.Initialize;
  Application.CreateForm(TStressAppForm, StressAppForm);
  Application.Run;
end.
