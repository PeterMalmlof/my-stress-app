unit TStressAppFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls,

  TGenAppPropUnit,      // Application Properties
  TMyLoadThreadUnit,    // Cpu Loading Thread
  TPmaLogUnit,          // Log Component
  TGenMemoUnit,         // Log Window
  TGenSliderUnit,       // Slider
  TGenProgressBarUnit,  // Progress Bar
  TWmMsgFactoryUnit,    // Message Factory
  TBkgThreadQueueUnit, Menus, TGenPopupMenuUnit;  // Background Thread

type
  TStressAppForm = class(TForm)
    PmaLog: TPmaLog;
    LogWin: TGenMemo;
    SetCpuLoad: TGenSlider;
    Cpu1Load: TGenProgressBar;
    Cpu2Load: TGenProgressBar;
    Cpu3Load: TGenProgressBar;
    Cpu4Load: TGenProgressBar;
    WmMsgFactory1: TWmMsgFactory;
    BkgThreadQueue1: TBkgThreadQueue;
    Cpu5Load: TGenProgressBar;
    Cpu6Load: TGenProgressBar;
    Cpu7Load: TGenProgressBar;
    Cpu8Load: TGenProgressBar;
    CpuLoadWant: TLabel;
    AppProp1: TAppProp;
    AppMenu: TGenPopupMenu;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PmaLogLog(sLine: String);
    procedure SetCpuLoadPosition(Sender: TGenSlider; Position: Single);
    procedure Log(Line: String);
    procedure Cpu1LoadDblClick(Sender: TObject);
    procedure Cpu2LoadDblClick(Sender: TObject);
    procedure Cpu3LoadDblClick(Sender: TObject);
    procedure Cpu4LoadDblClick(Sender: TObject);
    procedure Cpu5LoadDblClick(Sender: TObject);
    procedure Cpu6LoadDblClick(Sender: TObject);
    procedure Cpu7LoadDblClick(Sender: TObject);
    procedure Cpu8LoadDblClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure AppMenuPopup(Sender: TObject);
  private
    objTimer    : TTimer;    // Timer for Measuring Cpu Load
    objLoad     : integer;   // Cpu Load wanted
    objLoadAvg  : single;    // Average Cpu Load
    objRunning  : boolean;

    objCpuCount : integer;                // Number of Cpu's
    tLoad       : array of integer;       // Actual Load
    objThreads  : array of TMyLoadThread; // Current Threads
    objCpuOn    : array of boolean;       // Cpu is On/Off

    objUseLog  : boolean;
    objRight   : integer; // Right Border of Cpu Prgress Bars

    // Measure current Load

    procedure OnMyTimer(Sender : TObject);

    // Start All Threads

    procedure StartAllThreads;

    // Set Thread Loads

    procedure SetThreadsLoad;

    procedure OnMyClose(Sender : TObject);
  public

  end;

var
  StressAppForm: TStressAppForm;

implementation

{$R *.dfm}

uses
  TPmaCpuUtilsUnit,       // TGenCpuUnit
  TPmaClassesUnit;   // Class Management

const
  BRD          = 4;
  CPULOADWIDTH = 24;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TStressAppForm.FormCreate(Sender: TObject);
begin
  // Startup All Components

  objRunning := false;

  objCpuCount := TPmaCpuUtilsUnit.GetCpuCount;
  Cpu1Load.Visible := (objCpuCount > 0);
  Cpu2Load.Visible := (objCpuCount > 1);
  Cpu3Load.Visible := (objCpuCount > 2);
  Cpu4Load.Visible := (objCpuCount > 3);
  Cpu5Load.Visible := (objCpuCount > 4);
  Cpu6Load.Visible := (objCpuCount > 5);
  Cpu7Load.Visible := (objCpuCount > 6);
  Cpu8Load.Visible := (objCpuCount > 7);

  App.StartUp;
  App.pAutoSave := true;

  objUseLog := false;

  if objUseLog then
    LogWin.StartUp;

  SetCpuLoad.StartUp;

  Cpu1Load.StartUp;
  Cpu2Load.StartUp;
  Cpu3Load.StartUp;
  Cpu4Load.StartUp;
  Cpu5Load.StartUp;
  Cpu6Load.StartUp;
  Cpu7Load.StartUp;
  Cpu8Load.StartUp;

  objLoad             := 50;
  objLoadAvg := objLoad;

  SetCpuLoad.Position := objLoad;
  SetCpuLoad.Hint     := 'Load: ' + IntToStr(round(SetCpuLoad.Position)) + '%';
  CpuLoadWant.Caption := IntToStr(trunc(SetCpuLoad.Position)) + '%';
  Application.Title   := CpuLoadWant.Caption;
  self.Caption        := CpuLoadWant.Caption;

  // Set tLoad and Threads Array Length of the Cpu Count

  SetLength(tLoad,      objCpuCount);
  SetLength(objThreads, objCpuCount);
  SetLength(objCpuOn,   objCpuCount);

  // Start Threads

  StartAllThreads;

  // Start Measuring Timer

  objTimer          := TTimer.Create(nil);
  objTimer.Interval := 2000;
  objTimer.OnTimer  := OnMyTimer;
  objTimer.Enabled  := true;

  objRunning := true;

  Log(StringOfChar('-', 80));
end;
//------------------------------------------------------------------------------
//  Destroy Application
//------------------------------------------------------------------------------
procedure TStressAppForm.FormDestroy(Sender: TObject);
begin
  Log(StringOfChar('-', 80));

  objTimer.Free;

  BkgQueue.TerminatAllProcesses;

  // Write Window Rect

  App.ShutDown;

  SetLength(tLoad,      0);
  SetLength(objThreads, 0);
  SetLength(objCpuOn,   0);

  ClassFactory.SaveandClose;
end;
//------------------------------------------------------------------------------
//  Reload all processes
//------------------------------------------------------------------------------
procedure TStressAppForm.StartAllThreads;
var
  Ind   : integer;
begin
  Log('Loading Cpu Load Threads ' + IntToStr(objLoad) + '%');

  for Ind := 0 to objCpuCount - 1 do
    begin
      // Create new Thread

      objThreads[Ind] := TMyLoadThread.Create(objLoad, Ind);

      // Set Affinity to the current Cpu

      objThreads[Ind].pAffinity := Ind;

      objCpuOn[Ind] := true;
    end;
end;
//------------------------------------------------------------------------------
//  Reload all processes
//------------------------------------------------------------------------------
procedure TStressAppForm.SetThreadsLoad;
var
  Ind : integer;
begin
  Log('SetThreadsLoad ' + IntToStr(objLoad) + '%');

  for Ind := 0 to objCpuCount - 1 do
    begin
      // Create new Thread

      if objCpuOn[Ind] then
        objThreads[Ind].pLoad := objLoad
      else
        objThreads[Ind].pLoad := 0;
    end;
end;
//------------------------------------------------------------------------------
//  Timer Event
//------------------------------------------------------------------------------
procedure TStressAppForm.OnMyTimer(Sender : TObject);
var
  Ind : integer;
  CurLoad : single;
begin

  // Get Load for all Cores

  TPmaCpuUtilsUnit.GetCpuLoad(objCpuCount, tLoad);

  // Calculate Load for all Cores

  CurLoad := 0;
  for Ind := 0 to objCpuCount - 1 do
    CurLoad := CurLoad + tLoad[Ind];

  if (objCpuCount > 1) then
    CurLoad := CurLoad / objCpuCount;

  // Change Average by 10 %

  objLoadAvg := CurLoad;//objLoadAvg + 0.1 * (CurLoad - objLoadAvg);

  if Cpu1Load.Visible then
    begin
      Cpu1Load.Hint := 'Load ' + IntToStr(tLoad[0]) + '%';
      Cpu1Load.ValueCur := tLoad[0];
    end;

  if Cpu2Load.Visible then
    begin
      Cpu2Load.Hint := 'Load ' + IntToStr(tLoad[1]) + '%';
      Cpu2Load.ValueCur := tLoad[1];
    end;

  if Cpu3Load.Visible then
    begin
      Cpu3Load.Hint := 'Load ' + IntToStr(tLoad[2]) + '%';
      Cpu3Load.ValueCur := tLoad[2];
    end;

  if Cpu4Load.Visible then
    begin
      Cpu4Load.Hint := 'Load ' + IntToStr(tLoad[3]) + '%';
      Cpu4Load.ValueCur := tLoad[3];
    end;

  if Cpu5Load.Visible then
    begin
      Cpu5Load.Hint := 'Load ' + IntToStr(tLoad[4]) + '%';
      Cpu5Load.ValueCur := tLoad[4];
    end;

  if Cpu6Load.Visible then
    begin
      Cpu6Load.Hint := 'Load ' + IntToStr(tLoad[5]) + '%';
      Cpu6Load.ValueCur := tLoad[5];
    end;

  if Cpu7Load.Visible then
    begin
      Cpu7Load.Hint := 'Load ' + IntToStr(tLoad[6]) + '%';
      Cpu7Load.ValueCur := tLoad[6];
    end;

  if Cpu8Load.Visible then
    begin
      Cpu8Load.Hint := 'Load ' + IntToStr(tLoad[7]) + '%';
      Cpu8Load.ValueCur := tLoad[7];
    end;

  CpuLoadWant.Caption := IntToStr(round(SetCpuLoad.Position)) + '% (' +
                         IntToStr(round(objLoadAvg)) + ') ';

  CpuLoadWant.Left  := (self.ClientWidth - CpuLoadWant.Width) div 2;
  Application.Title := CpuLoadWant.Caption;
  self.Caption      := CpuLoadWant.Caption;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TStressAppForm.FormResize(Sender: TObject);
begin
  // CpuLoad Slider to the Left

  CpuLoadWant.Top  := BRD;

  SetCpuLoad.Left   := BRD * 3;
  SetCpuLoad.Width  := CPULOADWIDTH;
  SetCpuLoad.Top    := CpuLoadWant.Top + CpuLoadWant.Height + BRD;
  SetCpuLoad.Height := self.ClientHeight - BRD - SetCpuLoad.Top;

  CpuLoadWant.Left := (self.ClientWidth - CpuLoadWant.Width) div 2;

  objRight := SetCpuLoad.Left + SetCpuLoad.Width + BRD * 4;

  // Progress Bar is Placed to the Right of CpuLoad

  if Cpu1Load.Visible then
    begin
      Cpu1Load.Left   := objRight;
      Cpu1Load.Width  := CPULOADWIDTH - BRD * 2;
      Cpu1Load.Top    := SetCpuLoad.Top + CPULOADWIDTH div 2;
      Cpu1Load.Height := SetCpuLoad.Height - CPULOADWIDTH;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  if Cpu2Load.Visible then
    begin
      Cpu2Load.Left   := objRight;
      Cpu2Load.Width  := Cpu1Load.Width;
      Cpu2Load.Top    := Cpu1Load.Top;
      Cpu2Load.Height := Cpu1Load.Height;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  if Cpu3Load.Visible then
    begin
      Cpu3Load.Left   := objRight;
      Cpu3Load.Width  := Cpu1Load.Width;
      Cpu3Load.Top    := Cpu1Load.Top;
      Cpu3Load.Height := Cpu1Load.Height;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  if Cpu4Load.Visible then
    begin
      Cpu4Load.Left   := objRight;
      Cpu4Load.Width  := Cpu1Load.Width;
      Cpu4Load.Top    := Cpu1Load.Top;
      Cpu4Load.Height := Cpu1Load.Height;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  if Cpu5Load.Visible then
    begin
      Cpu5Load.Left   := objRight;
      Cpu5Load.Width  := Cpu1Load.Width;
      Cpu5Load.Top    := Cpu1Load.Top;
      Cpu5Load.Height := Cpu1Load.Height;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  if Cpu6Load.Visible then
    begin
      Cpu6Load.Left   := objRight;
      Cpu6Load.Width  := Cpu1Load.Width;
      Cpu6Load.Top    := Cpu1Load.Top;
      Cpu6Load.Height := Cpu1Load.Height;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  if Cpu7Load.Visible then
    begin
      Cpu7Load.Left   := objRight;
      Cpu7Load.Width  := Cpu1Load.Width;
      Cpu7Load.Top    := Cpu1Load.Top;
      Cpu7Load.Height := Cpu1Load.Height;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  if Cpu8Load.Visible then
    begin
      Cpu8Load.Left   := objRight;
      Cpu8Load.Width  := Cpu1Load.Width;
      Cpu8Load.Top    := BRD + CPULOADWIDTH div 2;
      Cpu8Load.Height := Cpu1Load.Height;
      Inc(objRight, BRD + CPULOADWIDTH);
    end;

  Inc(objRight, BRD*2);

  LogWin.Visible := objUseLog;

  if objUseLog then
    begin
      LogWin.Left   := objRight + BRD;
      LogWin.Width  := self.ClientWidth - LogWin.Left - BRD;
      LogWin.Top    := BRD;
      LogWin.Height := self.ClientHeight - BRD * 2;
    end;

    
  invalidate;
end;
//------------------------------------------------------------------------------
//  Callback from Logging
//------------------------------------------------------------------------------
procedure TStressAppForm.PmaLogLog(sLine: String);
begin
  if objUseLog then LogWin.Append(sLine);
end;
//------------------------------------------------------------------------------
//  CpuLoad Changed
//------------------------------------------------------------------------------
procedure TStressAppForm.SetCpuLoadPosition(Sender: TGenSlider;
  Position: Single);
begin
  Log('Set CpuLoad: ' + IntToStr(round(SetCpuLoad.Position)));
  objLoad := round(SetCpuLoad.Position);

  SetCpuLoad.Hint := 'CpuLoad: ' + IntToStr(round(SetCpuLoad.Position)) + '%';
  SetThreadsLoad;

  CpuLoadWant.Caption := IntToStr(round(SetCpuLoad.Position)) + '%';
  Application.Title := CpuLoadWant.Caption;
  self.Caption := CpuLoadWant.Caption;
  CpuLoadWant.Left := (self.ClientWidth - CpuLoadWant.Width) div 2;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TStressAppForm.Log(Line: String);
begin
  PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu1LoadDblClick(Sender: TObject);
begin
  objCpuOn[0] := not objCpuOn[0];
  if objCpuOn[0] then
    objThreads[0].pLoad := objLoad
  else
    objThreads[0].pLoad := 0;

  Log('CpuLoad ' + BoolToStr(objCpuOn[0], true));
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu2LoadDblClick(Sender: TObject);
begin
  objCpuOn[1] := not objCpuOn[1];
  if objCpuOn[1] then
    objThreads[1].pLoad := objLoad
  else
    objThreads[1].pLoad := 0;
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu3LoadDblClick(Sender: TObject);
begin
  objCpuOn[2] := not objCpuOn[2];
  if objCpuOn[2] then
    objThreads[2].pLoad := objLoad
  else
    objThreads[2].pLoad := 0;
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu4LoadDblClick(Sender: TObject);
begin
  objCpuOn[3] := not objCpuOn[3];
  if objCpuOn[3] then
    objThreads[3].pLoad := objLoad
  else
    objThreads[3].pLoad := 0;
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu5LoadDblClick(Sender: TObject);
begin
  objCpuOn[4] := not objCpuOn[4];
  if objCpuOn[4] then
    objThreads[4].pLoad := objLoad
  else
    objThreads[4].pLoad := 0;
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu6LoadDblClick(Sender: TObject);
begin
  objCpuOn[5] := not objCpuOn[5];
  if objCpuOn[5] then
    objThreads[5].pLoad := objLoad
  else
    objThreads[5].pLoad := 0;
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu7LoadDblClick(Sender: TObject);
begin
  objCpuOn[6] := not objCpuOn[6];
  if objCpuOn[6] then
    objThreads[6].pLoad := objLoad
  else
    objThreads[6].pLoad := 0;
end;
//------------------------------------------------------------------------------
// Enable/Disable Cpu
//------------------------------------------------------------------------------
procedure TStressAppForm.Cpu8LoadDblClick(Sender: TObject);
begin
  objCpuOn[7] := not objCpuOn[7];
  if objCpuOn[7] then
    objThreads[7].pLoad := objLoad
  else
    objThreads[7].pLoad := 0;
end;
//------------------------------------------------------------------------------
//  Paint Background
//------------------------------------------------------------------------------
procedure TStressAppForm.FormPaint(Sender: TObject);
var
  R : TRect;
  I : integer;
  Y : integer;
begin
  inherited;

  // Draw Lines             

  R.Left   := BRD;
  
  R.Right  := objRight - BRD * 3;
  R.Top    := Cpu1Load.Top;
  R.Bottom := Cpu1Load.Top + Cpu1Load.Height;

  self.Canvas.Pen.Style := psSolid;
  self.Canvas.Pen.Color := App.pForeColor;

  for I := 0 to 10 do
    begin
      Y := R.Top + round(I * (R.Bottom - R.Top) / 10);
      self.Canvas.MoveTo(R.Left, Y);
      self.Canvas.LineTo(R.Right, Y);
    end;
end;
//------------------------------------------------------------------------------
//  Make sure the windows isnt too small
//------------------------------------------------------------------------------
procedure TStressAppForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  if objRunning then
    begin
      if objUseLog then
        begin
          if (NewWidth < (LogWin.Left + 120)) then
            NewWidth := LogWin.Left + 120;
        end
      else
        begin
          if (NewWidth < objRight) then
            NewWidth := objRight;
        end;

      if NewHeight < 140 then
        NewHeight := 140;
  end
end;
//------------------------------------------------------------------------------
//  Pop Menu
//------------------------------------------------------------------------------
procedure TStressAppForm.AppMenuPopup(Sender: TObject);
var
  pMenu : TGenMenuItem;
begin

  AppMenu.Items.Clear;

  AppMenu.Font := App.pFont;
  AppMenu.BackColor := App.pBackColor;
  AppMenu.ForeColor := App.pForeColor;
  AppMenu.HighColor := App.pHighColor;

  pMenu := TGenMenuItem.Create(AppMenu);
  pMenu.Caption := 'Preferences ';
  AppMenu.Items.Add(pMenu);

  App.AddPrefMenu(AppMenu, pMenu, false, true);

  pMenu := TGenMenuItem.Create(AppMenu);
  pMenu.Caption := 'Exit';
  pMenu.OnClick := OnMyClose;
  AppMenu.Items.Add(pMenu);

end;
//------------------------------------------------------------------------------
//  Exit
//------------------------------------------------------------------------------
procedure TStressAppForm.OnMyClose(Sender : TObject);
begin
  self.Close;
end;
//------------------------------------------------------------------------------
//                                INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TStressAppForm);
end.
