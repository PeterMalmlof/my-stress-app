unit TGenCpuUnit;

interface

uses
  Windows;

  //----------------------------------------------------------------------------
  // Get Number of Processor Cores
  //----------------------------------------------------------------------------

  function  GetCpuCount : integer;

  //----------------------------------------------------------------------------
  // Get Cpu Top Speed
  //----------------------------------------------------------------------------

  function  GetCpuSpeed : integer;

  //----------------------------------------------------------------------------
  // Get Cpu Load per Core
  //----------------------------------------------------------------------------

  procedure GetCpuLoad(
              const CoreCount : integer;
              var   CpuLoad   : array of integer);

  // Get the Process with Highest Load

  function GetProcHighLoad: string;

//------------------------------------------------------------------------------
//                                IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  psAPI,
  tlhelp32;

//------------------------------------------------------------------------------
//  Definitions for later calls
//------------------------------------------------------------------------------
const
  SystemBasicInformation = 0;
  SystemProcessorPerformanceInformation = 8;

type
  TPDWord = ^DWORD;

//------------------------------------------------------------------------------
// TSystem_Basic_Information Record
//------------------------------------------------------------------------------
type
  TSystem_Basic_Information = packed record
    dwUnknown1                : DWORD;
    uKeMaximumIncrement       : ULONG;
    uPageSize                 : ULONG;
    uMmNumberOfPhysicalPages  : ULONG;
    uMmLowestPhysicalPage     : ULONG;
    uMmHighestPhysicalPage    : ULONG;
    uAllocationGranularity    : ULONG;
    pLowestUserAddress        : Pointer;
    pMmHighestUserAddress     : Pointer;
    uKeActiveProcessors       : ULONG;
    bKeNumberProcessors       : byte;
    bUnknown2                 : byte;
    wUnknown3                 : word;
  end;

//------------------------------------------------------------------------------
// TSystemProcessorPerformanceInformation Record
//------------------------------------------------------------------------------
type
  TSystemProcessorPerformanceInformation = packed record
    liIdleTime   : LARGE_INTEGER;
    liKernelTime : LARGE_INTEGER;
    liUserTime   : LARGE_INTEGER;
    reserved1    : LARGE_INTEGER;
    reserved2    : LARGE_INTEGER;
    reserved3    : LARGE_INTEGER; // Is defines as DWORD (Dont work)
  end;    // should be 48 bytes)


//------------------------------------------------------------------------------
// Address to ntlib.lib
//------------------------------------------------------------------------------
var  NtQuerySystemInformation: function(infoClass: DWORD;
        buffer: Pointer;
        bufSize: DWORD;
        returnSize: TPDword): DWORD; stdcall = nil;

//------------------------------------------------------------------------------
// TRue if Library is Initiated
//------------------------------------------------------------------------------
var NtQuerySystemInitiated : boolean = false;

//------------------------------------------------------------------------------
// Initiate The Process Unit
//------------------------------------------------------------------------------
procedure InternalInitNtDll; // Set up ntdll.dll
begin
  if (not NtQuerySystemInitiated) then
    begin
      NtQuerySystemInitiated := true;

      NtQuerySystemInformation :=
        GetProcAddress(GetModuleHandle('ntdll.dll'),
          'NtQuerySystemInformation');
    end;
end;
//------------------------------------------------------------------------------
//  Get Number of CPU's
//------------------------------------------------------------------------------
function GetCpuCount : integer;
var
  SysBaseInfo : TSystem_Basic_Information;
  status      : Longint;
begin
  InternalInitNtDll;

  status := NtQuerySystemInformation(
                  SystemBasicInformation,
                  @SysBaseInfo,
                  SizeOf(SysBaseInfo),
                  nil);
  if status = 0 then
    result := SysBaseInfo.bKeNumberProcessors
  else
    result := 1;
end;
//------------------------------------------------------------------------------
// Get Cpu Speed
//------------------------------------------------------------------------------
function GetCpuSpeed: integer;

  function RdTSC : int64; register;
  asm
    db   $0f, $31
  end;

var
  hF, T, et, sc, r : int64;
begin
  QueryPerformanceFrequency(hF);  // HiTicks / second
  QueryPerformanceCounter(T);     // Determine start HiTicks
  et := T + hF;                   // (Cycles are passing, but we can still USE them!)
  sc := RdTSC;                    // Get start cycles
  repeat                          // Use Hi Perf Timer to loop for 1 second
    Sleep(1);
    QueryPerformanceCounter(T);   // Check ticks NOW
  until (T >= et);                //  Break the moment we equal or exceed et
  r := RdTSC - sc;                // Get stop cycles and calculate result

  result := r div 1000000;
end;
//------------------------------------------------------------------------------
//  Get Cpu Load for all Processors individual
//------------------------------------------------------------------------------
const
  MAXCORES = 7;
  
var
  LastSysTime   : int64;                        // Last System Time
  LastIdleTime  : array [0..MAXCORES] of int64; // Last Idle Time

procedure GetCpuLoad(
              const CoreCount : integer;
              var   CpuLoad   : array of integer);
var
  CurSysTime : int64;     // Current System Time in Ticks
  status     : Longint;
  retval     : DWORD;
  Ind        : integer;

  SysPerfInfo   : array [0..MAXCORES] of TSystemProcessorPerformanceInformation;
  CurIdleTime   : array [0..MAXCORES] of int64;

  DeltaSysTime   : int64;
begin
  if (CoreCount < 1) or (CoreCount > MAXCORES) then EXIT;

  InternalInitNtDll;

  // TheLog.Log('Cores ' + IntToStr(CurCoreCount) +
  //            ' Size ' + IntToStr(SizeOf(SysPerfInfo)));

  // Get current System Time in Ticks (in mSeconds)

  CurSysTime := Windows.GetTickCount();

  // Get System Performance Information

  status := NtQuerySystemInformation(
                SystemProcessorPerformanceInformation,
                @SysPerfInfo,
                SizeOf(SysPerfInfo),
                @retval);


  DeltaSysTime := CurSysTime - LastSysTime;
  LastSysTime := CurSysTime;

  // Get Current Values

  if status = 0 then
    begin
      for Ind := 0 to CoreCount - 1 do
        if (retval >= ((Ind+1) *
            sizeof(TSystemProcessorPerformanceInformation))) then
        begin
          CurIdleTime[Ind] :=
              SysPerfInfo[Ind].liIdleTime.HighPart * 4294967295 +
                      SysPerfInfo[Ind].liIdleTime.LowPart;

          // Divide by 10000 to align it with System Tick Time

          CurIdleTime[Ind] := Round(CurIdleTime[Ind] / 10000);

          // Calculate delta on all : Cur - Old

          CpuLoad[Ind] := Round(100.0 -
           ( ( (CurIdleTime[Ind] - LastIdleTime[Ind]) / DeltaSysTime) * 100.0));

          LastIdleTime[Ind] := CurIdleTime[Ind];
        end;
    end;
end;
//------------------------------------------------------------------------------
// Get the Process with Highest Load
//------------------------------------------------------------------------------
function GetProcHighLoad: string;
var
  hProcessSnap  : THandle;
  pe32          : PROCESSENTRY32;
  nLoad : integer;

  PID : DWORD;
  sTmp          : string;
begin
  result := '';
  nLoad  := -1;

  // Enumerate all Processes

  hProcessSnap := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if (hProcessSnap = INVALID_HANDLE_VALUE) then
    begin
      //TheLog.Log('Cant open CreateToolhelp32Snapshot');
      Exit;
    end;

  // Set the size of the structure before using it.

  pe32.dwSize := sizeof(PROCESSENTRY32);

  // Retrieve information about the first process,

  if (not Process32First(hProcessSnap, pe32)) then
    begin
      //TheLog.Log('Cant get the first process (Process32First');
      CloseHandle(hProcessSnap); // clean the snapshot object
      Exit;
    end;

  repeat

    // Find an Object with this Process Id

    PID := pe32.th32ProcessID;

    // Get the Load of this Process


    // Get the Next Process

  until (not Process32Next( hProcessSnap, pe32 ));

  // Close the Snapshot Handle

  CloseHandle( hProcessSnap );


end;
end.
