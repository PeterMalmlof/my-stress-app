unit TMyLoadThreadUnit;

interface
uses
  Classes, Windows, SysUtils,

  TBkgThreadQueueUnit;

type
  TMyLoadThread = class(TBkgThread)
  private
    objLoad      : integer;
    objLoadTick  : Cardinal;
    objSleepTick : Cardinal;
  protected
    procedure CalcTimes;

    function GetLoadTick  : cardinal;
    function GetSleepTick : cardinal;

    function  GetLoad : integer;
    procedure SetLoad(const Value : integer);

    procedure Execute; override;
  public
    constructor Create(const nLoad, Id : integer);

    property pLoad : integer read GetLoad write SetLoad;
  end;

implementation
uses
  TPmaClassesUnit;

const
  CycleLength = 100;

//------------------------------------------------------------------------------
//  Create Thread
//------------------------------------------------------------------------------
constructor TMyLoadThread.Create(const nLoad, Id : integer);
begin
  inherited Create(Id);

  // The Thread is write locked by the inherited constructor

  // We dont need a real reply

  objResult := 0;
  objReply  := false;

  // Load is the % Load needed for this Process

  objLoad := nLoad;

  CalcTimes;

  self.Priority := tpNormal;

  // It will be write locked until AfterConstruction is called
end;
//------------------------------------------------------------------------------
//  WRITE LOCKED: Calculate all Timing
//------------------------------------------------------------------------------
procedure TMyLoadThread.CalcTimes;
begin
  // Load Tick is the nr of MilliSeconds  we want 100 Load

  objLoadTick  := objLoad * CycleLength div 100;

  // Sleep Ticks is the nr of MilliSeconds  we want 0 Load (sleep)

  objSleepTick := CycleLength - objLoadTick;

  Log('Load:  ' + IntToStr(objLoad) + '%');
  Log('100%:  ' + IntToStr(objLoadTick) + 'mS');
  Log('Sleep: ' + IntToStr(objSleepTick) + 'mS');
end;
//------------------------------------------------------------------------------
//  Get Load Ticks
//------------------------------------------------------------------------------
function TMyLoadThread.GetLoadTick  : cardinal;
begin
  objCS.BeginRead;
  result := objLoadTick;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
//  Get Sleep Ticks
//------------------------------------------------------------------------------
function TMyLoadThread.GetSleepTick : cardinal;
begin
  objCS.BeginRead;
  result := objSleepTick;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
//  Get Current Load
//------------------------------------------------------------------------------
function TMyLoadThread.GetLoad : integer;
begin
  objCS.BeginRead;
  result := objLoad;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
//  Set Current Load
//------------------------------------------------------------------------------
procedure TMyLoadThread.SetLoad(const Value : integer);
begin
  objCS.BeginWrite;
  objLoad := Value;
  CalcTimes;
  objCS.EndWrite;
end;
//------------------------------------------------------------------------------
//  Execute Thread
//------------------------------------------------------------------------------
procedure TMyLoadThread.Execute;
var
  NextTick  : Cardinal;
  R,S,T,U   : double;
  T1, T2    : Cardinal;
  NextSleep : integer;
  lTick, sTick : cardinal;

begin
  U := 0;
  R := 0;

  while not self.Terminated do
    begin
      objCS.BeginRead;
      lTick := objLoadTick;
      sTick := objSleepTick;
      objCS.EndRead;

      // Calculate Next Tick to stop

      T1 := Windows.GetTickCount;
      NextTick := T1 + lTick;
      while (Windows.GetTickCount < NextTick) and
            (not self.Terminated) do
        begin
          // Totally Nonsense
          R := 1.01;
          S := sqrt(R);
          R := R + 0.002;
          T := sqrt(R);
          R := U + T + S;
        end;

      T2 := Windows.GetTickCount;

      // Sleep for the remaining time of CycleLength. Adjust for
      // Tick precision (aint that good on some Computers)

      if (lTick <> 0) then
        NextSleep := round(sTick * (T2-T1) / lTick)
      else
        NextSleep := sTick;

      if (NextSleep > 0) and (not self.Terminated) then
        Sleep(NextSleep);
    end;

  // Just to make the compiler happy

  U := R;
  Log('Factor ' + FloatToStr(U));
end;
//------------------------------------------------------------------------------
//                                 INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMyLoadThread);
end.

